var express = require('express'),
	router = express.Router(),
	request = require('request'),
	rp = require('request-promise'),
	modelCarrinho = require('../model/carrinho');

// GET checkout
router.get('/:id', function (req, res, next) {
	modelCarrinho.Carrinho.findById(req.params.id, function (error, docCarrinho) {

		let aux = ''

		bodyNew = '{'
		bodyNew +='	"api_key": "ak_test_Fdo1KyqBTdnTFeLgBhkgRcgm9hwdzd",'	
		bodyNew +='	"amount": 21000,'
		bodyNew +='	"card_number": "4111111111111111",'
		bodyNew +='	"card_cvv": "123",'
		bodyNew +='	"card_expiration_date": "0922",'
		bodyNew +='	"card_holder_name": "João das Neves",'
		bodyNew +='	"customer": {'
		bodyNew +='	  "external_id": "#3311",'
		bodyNew +='	  "name": "João das Neves Braulio",'
		bodyNew +='	  "type": "individual",'
		bodyNew +='	  "country": "br",'
		bodyNew +='	  "email": "joaodasneves@got.com",'
		bodyNew +='	  "documents": ['
		bodyNew +='		{'
		bodyNew +='		  "type": "cpf",'
		bodyNew +='		  "number": "00000000000"'
		bodyNew +='		}'
		bodyNew +='	  ],'
		bodyNew +='	  "phone_numbers": ["+5511999998888", "+5511888889999"],'
		bodyNew +='	  "birthday": "1965-01-01"'
		bodyNew +='	},'
		bodyNew +='	"billing": {'
		bodyNew +='	  "name": "João das Neves",'
		bodyNew +='	  "address": {'
		bodyNew +='		"country": "br",'
		bodyNew +='		"state": "sp",'
		bodyNew +='		"city": "Cotia",'
		bodyNew +='		"neighborhood": "Rio Cotia",'
		bodyNew +='		"street": "Rua Matrix",'
		bodyNew +='		"street_number": "9999",'
		bodyNew +='		"zipcode": "06714360"'
		bodyNew +='	  }'
		bodyNew +='	},'
		bodyNew +='	"shipping": {'
		bodyNew +='	  "name": "Neo Reeves",'
		bodyNew +='	  "fee": 1000,'
		bodyNew +='	  "delivery_date": "2000-12-21",'
		bodyNew +='	  "expedited": true,'
		bodyNew +='	  "address": {'
		bodyNew +='		"country": "br",'
		bodyNew +='		"state": "sp",'
		bodyNew +='		"city": "Cotia",'
		bodyNew +='		"neighborhood": "Rio Cotia",'
		bodyNew +='		"street": "Rua Matrix",'
		bodyNew +='		"street_number": "9999",'
		bodyNew +='		"zipcode": "06714360"'
		bodyNew +='	  }'
		bodyNew +='	},'

		for (let index = 0; index < docCarrinho.itens.length; index++) {
			const element = docCarrinho.itens[index];
			if ((index)+1 == docCarrinho.itens.length ) {
				aux += ' { "id": "'+element._id+'" ,"title": "'+element.produto+'" , "quantity": '+element.quantidade+', "unit_price": '+parseInt(element.precounitario)+' ,"tangible": true }  '
			} else {
				aux += ' { "id": "'+element._id+'" ,"title": "'+element.produto+'" , "quantity": '+element.quantidade+', "unit_price": '+parseInt(element.precounitario)+' ,"tangible": true }, '
			}	
		}
		bodyNew +='	"items": ['+aux+'] '
		bodyNew +='}'

		checkout(bodyNew).then(dataCheckout => {
			res.status(200).json(dataCheckout);
		}).catch(error => {
			res.status(400).json(error);
		})

	});
});

module.exports = { router: router, path: '/v1/checkouts' };

checkout = (bodyNew) => {

	return new Promise((resolve, reject) => {

		var options = {
			method: 'POST',
			uri: 'https://api.pagar.me/1/transactions',
			body: bodyNew,
			headers: {
				'Content-Type': 'application/json',
				'Accept': 'application/json'
			},
			strictSSL: false,
			rejectUnauthorized: false
		}

		rp(options)
			.then(data => {
				resolve(data);
			})
			.catch(err => {
				reject(err);
			})

	})


}

var express = require('express'),
	router = express.Router(),
	modelCarrinho = require('../model/carrinho'),
	modelProduto = require('../model/produto');

// GET carrinho
router.get('/', function (req, res, next) {

	modelCarrinho.Carrinho.find({}, function (error, docCarrinhos) {
		if (error) {
			res.status(500).json(error);
		} else {
			res.status(200).json(docCarrinhos);
		}
	});

});

// POST carrinho
router.post('/', function (req, res, next) {

	let body = req.body
	body.valorbruto = 0

	carrinho(body, 'ADICIONA', (docCarrinhos, error) => {
		if (error) {
			res.status(400).json(error);
		} else {
			res.status(201).json(docCarrinhos);
		}
	})
});

// DELETE carrinho
router.delete('/:id/:produto/:quantidade', function (req, res, next) {

	let body = req.params

	carrinho(body, 'REMOVE', (docCarrinhos, error) => {
		if (error) {
			res.status(400).json(error);
		} else {
			res.status(200).json(docCarrinhos);
		}
	})
});

module.exports = { router: router, path: '/v1/carrinhos' };

// CRIA O CARRINHO COM ITENS
async function carrinho(body, operacao, callback) {

	var newBody = { valorbruto: 0, valorliquido: 0, itens: [] }
	var dados = body

	var valorLiquidoA = 0
	var valorLiquidoB = 0
	var valorLiquidoC = 0

	var percAQtd = 0.01 // 1%  no fator A
	var percBQtd = 0.05 // 5%  no fator B
	var percCQtd = 0.10 // 10% no fator C

	var limitAQtd = 5 // limite por fator 5%
	var limitBQtd = 15 // limite por fator 15%
	var limitCQtd = 30 // limite por fator 30%
	var limitTotal = 30 // limite total carrinho 30%


	if (operacao === 'REMOVE') {

		modelCarrinho.Carrinho.findById(body.id, function (err, docCarrinho) {
			if (err) return callback(null, err)

			var mapDoc = docCarrinho.itens.map(function (el) { return el.produto })
			var posicao = mapDoc.indexOf(body.produto)
			var count = 0

			docCarrinho.itens[posicao].quantidade -= body.quantidade

			for (let index = 0; index < docCarrinho.itens.length; index++) {
				const element = docCarrinho.itens[index];
				
				findOne(modelProduto.Produto, { nome: element.produto }).then(docProduto => {

					// VALIDA SE O PRODUTO EXISTE NO CADASTRO
					if (docProduto != null) {

						//APLICANDO O DESCONTO
						switch (docProduto.fator) {
							case "A":
								resultDesconto = calculaDesconto(docProduto.fator, docProduto.valor, element.quantidade, percAQtd, limitAQtd)
								valorLiquidoA = resultDesconto.valorLiquido
								docCarrinho.itens[index].precounitario = resultDesconto.precounitario
								break;
							case "B":
								resultDesconto = calculaDesconto(docProduto.fator, docProduto.valor, element.quantidade, percBQtd, limitBQtd)
								valorLiquidoB = resultDesconto.valorLiquido
								docCarrinho.itens[index].precounitario = resultDesconto.precounitario 
								break;
							case "C":
								resultDesconto = calculaDesconto(docProduto.fator, docProduto.valor, element.quantidade, percCQtd, limitCQtd)
								valorLiquidoC = resultDesconto.valorLiquido
								docCarrinho.itens[index].precounitario = resultDesconto.precounitario
								break;
						}

						newBody.valorbruto += docProduto.valor * element.quantidade

						if ((index+1) == docCarrinho.itens.length) {

							docCarrinho.valorbruto = newBody.valorbruto
							docCarrinho.valorliquido = valorLiquidoA + valorLiquidoB + valorLiquidoC

							docCarrinho.save(function (err, docCarrinho) {
								if (err) return callback(null, err)
								callback(docCarrinho)
							})
						}
					}


				})

			}

		})

	} else if (operacao === 'ADICIONA') {

		for (let index = 0; index < dados.itens.length; index++) {
			const element = dados.itens[index];
			docProduto = await findOne(modelProduto.Produto, { nome: element.produto })

			// VALIDA SE O PRODUTO EXISTE NO CADASTRO
			if (docProduto != null) {

				//APLICANDO O DESCONTO
				switch (docProduto.fator) {
					case "A":
						resultDesconto = calculaDesconto(docProduto.fator, docProduto.valor, element.quantidade, percAQtd, limitAQtd)
						valorLiquidoA = resultDesconto.valorLiquido
						element.precounitario = resultDesconto.precounitario
						break;
					case "B":
						
						resultDesconto = calculaDesconto(docProduto.fator, docProduto.valor, element.quantidade, percBQtd, limitBQtd)
						valorLiquidoB = resultDesconto.valorLiquido
						element.precounitario = resultDesconto.precounitario
						break;
					case "C":
						resultDesconto = calculaDesconto(docProduto.fator, docProduto.valor, element.quantidade, percCQtd, limitCQtd)
						valorLiquidoC = resultDesconto.valorLiquido
						element.precounitario = resultDesconto.precounitario
						break;
				}

				newBody.valorbruto += docProduto.valor * element.quantidade
				newBody.itens.push(element)
			}
		}

		newBody.valorliquido = valorLiquidoA + valorLiquidoB + valorLiquidoC

		if (newBody.itens.length > 0) {
			new modelCarrinho.Carrinho(newBody).save(function (error, docCarrinhos) {
				if (error) {
					callback(error)
				} else {
					callback(docCarrinhos)
				}
			});
		} else {
			callback(null, { message: "Produtos não encontrados no nosso cadastro!" })
		}

	}

}

// BUSCA O REGISTRO NO BANCO
findOne = (model, filter) => {
	return new Promise((resolve, reject) => {
		model.findOne(filter, function (error, doc) {
			if (error) {
				reject(doc)
			} else {
				resolve(doc)
			}
		});
	})
}

findById = (model, filter) => {
	return new Promise((resolve, reject) => {
		model.findById(filter, function (error, doc) {
			if (error) {
				reject(doc)
			} else {
				resolve(doc)
			}
		});
	})
}

calculaDesconto = (fator, valor, quantidade, percQtd, limitQtd) => {

	var descontoBruto = 0
	var valorBruto = 0
	var valorLiquido = 0
	var descontoAjustado = 0
	var precounitario = 0

	valorBruto = (valor * quantidade)
	descontoBruto = (valor * quantidade) * (quantidade * percQtd)
	descontoAjustado = (descontoBruto / valorBruto) > (limitQtd / 100) ? valorBruto * (limitQtd / 100) : descontoBruto
	valorLiquido = valorBruto - descontoAjustado
	precounitario = valorLiquido / quantidade

	return { valorLiquido: valorLiquido, precounitario: precounitario }

}
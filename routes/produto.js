var express 		= require('express'),
    router  		= express.Router(),
    modelProduto	= require('../model/produto');

// GET produto
router.get('/', function(req, res, next) {
  modelProduto.Produto.find({},function(error,docProdutos) {
		if (error) {
      res.status(500).json(error);
	 	} else {
      res.status(200).json(docProdutos);
	 	}
	});
});

// POST produto
router.post('/', function(req, res, next) {
  new modelProduto.Produto(req.body).save(function(error,docProdutos) {
		if (error) {
			res.status(400).json(error);
		} else {
			res.status(201).json(docProdutos);
		}
	});
});

module.exports = { router: router, path: '/v1/produtos' };

# Desafio-Backend
Passo-a-Passo

* Instalação MongoDb
* Iniciar o serviço do MongoDb
* Na pasta do projeto execute npm install
* Na pasta do projeto execute npm start
* O node esta rodando na porta 3000

### Produto

* Cria Produto
```
POST /api/v1/produtos
{ "nome": "Caneta", "descricao": "Caneta ponta fina azul", "imagem": "canetafinaazul.jpg", "valor": 10, "fator": "C" }
```

* Lista Produto
```
GET /api/v1/produtos
[
    {
        "_id": "5b9fd38f66cf443adc07a4ec",
        "nome": "Caneta",
        "descricao": "Caneta ponta fina azul",
        "imagem": "canetafinaazul.jpg",
        "valor": 10,
        "fator": "C",
        "__v": 0
    },
    {
        "_id": "5b9fd3c866cf443adc07a4ed",
        "nome": "Lapis",
        "descricao": "Lapis 6b",
        "imagem": "lapis6b.jpg",
        "valor": 8,
        "fator": "B",
        "__v": 0
    },
    {
        "_id": "5b9fd3e566cf443adc07a4ee",
        "nome": "Borracha",
        "descricao": "Borracha branca",
        "imagem": "borrachabranca.jpg",
        "valor": 5,
        "fator": "A",
        "__v": 0
    }
]
```

### Carrinho

* Adiciona Carrinho
```
POST /api/v1/carrinhos
{ "itens":[{ "produto": "Caneta", "quantidade": 5 }, { "produto": "Borracha", "quantidade": 3}, { "produto": "Lapis", "quantidade": 1}] }
```

* Lista Carrinho
```
GET /api/v1/carrinhos
[
    {
        "_id": "5ba172258ba98026f018fc39",
        "valorbruto": 48,
        "valorliquido": 35.6,
        "itens": [
            {
                "_id": "5ba172258ba98026f018fc3c",
                "produto": "Caneta",
                "quantidade": 4,
                "precounitario": 7
            },
            {
                "_id": "5ba172258ba98026f018fc3b",
                "produto": "Borracha",
                "quantidade": 3,
                "precounitario": 4.8500000000000005
            },
            {
                "_id": "5ba172258ba98026f018fc3a",
                "produto": "Lapis",
                "quantidade": 1,
                "precounitario": 7.6
            }
        ],
        "__v": 0
    }
]
```

* Remover item do Carrinho
```
DELETE /api/v1/carrinhos/:id/:produto/:quantidade
       /api/v1/carrinhos/5ba049ae22897e1c208e4677/Caneta/1
[
    {
        "_id": "5ba172258ba98026f018fc39",
        "valorbruto": 48,
        "valorliquido": 35.6,
        "itens": [
            {
                "_id": "5ba172258ba98026f018fc3c",
                "produto": "Caneta",
                "quantidade": 4,
                "precounitario": 7
            },
            {
                "_id": "5ba172258ba98026f018fc3b",
                "produto": "Borracha",
                "quantidade": 3,
                "precounitario": 4.8500000000000005
            },
            {
                "_id": "5ba172258ba98026f018fc3a",
                "produto": "Lapis",
                "quantidade": 1,
                "precounitario": 7.6
            }
        ],
        "__v": 0
    }
]
```
* Checkout Carrinho - Integrando com pagar.me
```
GET /api/v1/checkouts/:id
    /api/v1/carrinhos/5ba049ae22897e1c208e4677/Caneta/1
{ "itens":[{ "produto": "Caneta", "quantidade": 5 }, { "produto": "Borracha", "quantidade": 3}, { "produto": "Lapis", "quantidade": 1}] }
```
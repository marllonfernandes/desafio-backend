var mongoose	= require('mongoose'),
uniqueValidator = require('mongoose-unique-validator');
	
mongoose.connect('mongodb://localhost:27017/desafiobackend'),
db = mongoose.connection

db.on('error',console.error.bind(console,'Error ao conectar no banco'));
db.once('open',function() {

	// SCHEMA PRODUTO
	var produtoSchema = mongoose.Schema({

		nome: { 
					type: String, 
					required: true,
					unique: true,
					index: true
				},
		descricao: { 
					type: String, 
					required: false,
					index: true
				},
		imagem: { 
					type: String, 
					required: false 
				},
		valor: {	type: Number, 
					required: true, 	 
				},
		fator: { 
					type: String, 
					required: true, 
					enum: ['A', 'B', 'C'] 
				}

	});

	// APLICA A VALIDACAO
	produtoSchema.plugin(uniqueValidator);

	// MODEL PRODUTO
	exports.Produto = mongoose.model('Produto',produtoSchema);	
	
});
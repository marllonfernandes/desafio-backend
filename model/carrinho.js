var mongoose	= require('mongoose')
	
mongoose.connect('mongodb://localhost:27017/desafiobackend'),
db = mongoose.connection

db.on('error',console.error.bind(console,'Error ao conectar no banco'));
db.once('open',function() {

	// SCHEMA CARRINHO
	var carrinhoSchema = mongoose.Schema({

		valorbruto: { type: Number },
		valorliquido: { type: Number },
		itens: [{ produto: String, quantidade: Number, precounitario: Number }]

	});

	// MODEL CARRINHO
	exports.Carrinho = mongoose.model('Carrinho',carrinhoSchema);	
	
});
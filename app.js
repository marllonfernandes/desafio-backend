var createError = require('http-errors'),
    express     = require('express'),
    path        = require('path'),
    cookieParser= require('cookie-parser'),
    logger      = require('morgan'),
    fs          = require('fs'),
    filesRoutes = fs.readdirSync(__dirname + '/routes'),
    app         = express();

// create a write stream (in append mode)
var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})

app.use(logger('dev', {stream: accessLogStream}))
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// list as routes
filesRoutes.forEach(element => {
  let namefile = element.split('.')
  let imp = require('./routes/' + namefile[0])
  app.use('/api'+imp.path, imp.router);
});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;